# Compte rendu conseil de classe S7 2020-02-14

## Résultat du sondage
Moyenne globale des notes du sondage (tout modules mélangés)

![moyenne sondage](./images/sondage_moyenne.png)
> Bleu: moyenne globale

# Techniques de l'Internet Dynamique, Architecture et Langages

Nombre d'assimilation : **1**

## Retour de l'enseignant
Retour positif de la part du prof avec de meilleurs résultats que l’année dernière.

## Modalités 2ème session
### DS
Un DS qui **annule et remplace** le DS précédent.
Intégration d’une partie de la note de projet dans le DS de rattrapage (voir le fichier tableur compliqué du prof).
### TP
Conservation des notes de projet vu qu'elles sont favorables à tout le monde.

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_tidal.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module

# Systèmes d'exploitation et programmation concurrente
Nombre d'assimilations : **3**

## Retour de l'enseignant

Très satisfait, de l’entraide entre les élèves, bonne promotion et bonne ambiance.

## Modalités 2ème session
### DS
Un DS de 2 heures qui **annule et remplace** la note de DS
### TP
Un DS d'1 heure qui **annule et remplace** la note de TP

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_sys.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module

# Architecture et protocoles réseaux pour IoT

Nombre d'assimilation : **Pas d'assimilation**

## Retour de l'enseignant

Il faut plus d’implications pendant les TP : ce qui explique le manque de temps durant le projet.
Les groupes travaillent plutôt bien.

Changement de carte l'année prochaine (Adieu les Techno Innov qui bug). Les cartes choisies sont les MicroBit qui sont programmable en python.

## Modalités 2ème session
### DS
QCM sur 1h.

### TP
Mini projet sur 8h et pour ceux qui doivent valider le DS. 
Il y a 3 groupes de 4 et vous êtes libres de la constitution (vous pouvez mélanger les groupes).

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_iot.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module

# Projet Transversal

## Retour des enseignants et docteurs

Bon investissement et participation de la part des élèves, tout le monde valide.

Plutôt positivement surpris, ne pensaient pas qu’on finirait les projets, satisfaits de l’autonomie et de la débrouillardise.

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_trans.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module


# Management de projet

## Retour de l'enseignant

M.Barbieri est satisfait, il gère bien le groupe malgré qu’ils soient trop nombreux parfois.

En agilité peu d’implication de la part des élèves, beaucoup de ronchonnement, globalement déçu de la capacité d’écoute et de travail de la promo. 

Pour l'histoire des supports de cours en agilité le représentant de l'ITII ou IRI ou AFPI ou Ascanis est très clerc sur le fait que l'enseignant doit donner ses slides dès le début.

## Modalités 2ème session
### Agilité DS
Un DS qui **annule et remplace**

### Agilité TP

Une étude individuelle à effectuer, avec deux sujets à faire en dehors des heures d’école, avec un dossier (maximum 20 diapos à proposer, qui fait office de rapport et de diapo pour la soutenance) à présenter et un oral (15 minutes de présentation et 5 minutes de questions, avec 3-4 jours de charge de travail à peu près. L’oral se fera à CPE. Environ mi-avril, conseil d’étaler la charge de travail jusque là.

### Management DS
Un DS qui **annule et remplace**

### Management TP
Un dossier à rendre avec un oral

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_gprojet.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module

# Gestion financière

Nombre d'assimilations : **2**

## Retour de l'enseignant

Promotion intéressante et impliquée

## Modalités 2ème session
### DS
Il y aura un autre DS qui **annule et remplace**.

Le prof conseil **très fortement** de rebosser les exercices faits en cours. (Avec un clin d'oeil)

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_gfinance.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module


# Anglais

## Retour de l'enseignant

Pas de remarques mais les profs sont globalement satisfaits, bagarres à éviter en cours d’anglais devant les profs.

Un peu deg de n'être que la 3ème matière la mieux noté. (Je crois)

## Modalités 2ème session
CV + Lettre de motivation + Vidéo à faire

Il y aura plus de détails par mail pour les personnes concernées.

## Résultat du sondage
Moyenne du module comparée à la moyenne globale

![moyenne sondage](./images/sondage_anglais.png)
> Bleu: moyenne globale
> 
> Rouge: moyenne du module

# Autres infos

## Mission à l’étranger
Pour aller en Colombie, comme le pays est en zone jaune, le point de départ pour ce stage c’est Oscar. Mais surtout, c’est M.Pignault qui donne le dernier mot.

## Mise à jour OS
Comme vous le savez, nous avons 2 systèmes d'exploitation à CPE: Windows 7 et Ubuntu 14.04 qui sont tous les deux dépréciés..

Une mise à jour est prévu d'après le directeur des études (mais pas de date actuellement)

B.Clerc, B.Ekinci, K.Decaderincourt